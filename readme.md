- Simulating iterated prisoner's dilemma games. Using [axelrod](https://axelrod.readthedocs.io/en/stable/index.html), [nashpy](https://nashpy.readthedocs.io/en/stable/) and Keras for custom learning model.
- [notebook preview](https://micstan.gitlab.io/share_zone/learn_prisoners_strategies.html)

**learn_prisoners_strategies.ipynb**

- Make a new player that learns to predict oponenets strategies based on their type and mimics the same choice. I use Keras to build minimalistic network that will learn as it plays.
- Simulate classic tournament and see how the new player is doing.
- Look at the model loss for each strategy and check how difficult it was to learn it.
- Propose a new way to run repeaded Prisoner's Dilema simulations:
    - open group setup - each round every player can play with any other
    - probability of beeing chosen changes from round to round based on the past choices - "Cooperation" increases the probability while "Defection" decreases it.
